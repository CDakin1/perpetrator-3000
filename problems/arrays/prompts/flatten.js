'use strict';

// flatten a nested array into a flat (1D) array
const flatten = (arrays, result) => {
  result = result || [];
  arrays.forEach((el) => {
    if (Array.isArray(el, result)) {
      flatten(el);
    } else {
      result.push(el);
    }
  });
  return result;
};

module.exports = { flatten };
