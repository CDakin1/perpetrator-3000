'use strict';

// returns an array without duplicates
const removeDuplicates = (array) => {
  const obj = {}, result = [];
  array.forEach(el => {
    if (!obj[el]) {
      obj[el] = 'added';
      result.push(el);
    }
  });
  return result;
};

module.exports = { removeDuplicates };
