'use strict';

// determine if every element in the array passes a truth test
const every = (array, truthTest) => {
  let result = true;
  array.forEach(el => {
    if (!truthTest(el)) {
      result = false;
    }
  });
  return result;
};

module.exports = { every };
